# Generated by Django 3.2.20 on 2023-09-03 01:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("prepayments", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="projectprepaymentdetail",
            name="overdraft_amount_allowed",
            field=models.PositiveIntegerField(
                blank=True,
                help_text="When set, this is the overdraft amount allowed for the project. Only works when there is at least one active fund with a positive balance.",
                null=True,
            ),
        ),
    ]
